/**
 * 
 */
package cern.eio.web.resources;

/**
 * @author aformic
 *
 */
public class LinkNames {
	
    public static final String PATH_SEPARATOR = "/";
    public static final String EIODS = PATH_SEPARATOR + "datasets";	
}
