package cern.eio.web.interceptors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.util.StopWatch;
import cern.eio.svc.annotations.LogAction;

 
////@////Aspect
public class ProfilingAspect {
 
//	@Pointcut(value="execution(public * *(..))")
	public void anyPublicMethod() {
		System.out.println("Calling a public method");
	}
 
//	@Around("anyPublicMethod() && @annotation(logAction)")
	public Object logAction(ProceedingJoinPoint pjp, LogAction logAction) throws Throwable {
 
		// Do what you want with the actionperformed
		String actionPerformed = logAction.actionPerformed();
 
		// Do what you want with the join point arguments
		for ( Object object : pjp.getArgs()) {
			System.out.println(object);
		}
 
		return pjp.proceed();
	}
	
////	@///Around("cern.eio.svc.repositories.impl.EiDAOImpl.findEventsGuidsFromInputFile(..)")
	public Object profile(ProceedingJoinPoint pjp) throws Throwable {
		 
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start("profiling");
		Object retval = pjp.proceed();
		stopWatch.stop();
		return retval;
	}

}
