/**
 * 
 */
package cern.eio.web.controllers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import af.cea.web.exceptions.ConddbWebException;
import cern.eio.data.views.EventData;
import cern.eio.svc.repositories.EioDAO;
import cern.eio.web.resources.LinkNames;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * @author aformic
 *
 */
@Path(LinkNames.EIODS)
@Controller
@Api(value = LinkNames.EIODS)
public class EioRestController extends af.cea.web.controllers.BaseController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EioDAO eioDao;


	@POST
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("/guids")
	@ApiOperation(value = "Finds GUIDs by stream using a list of run/event numbers", notes = "Search for a list of GUIDS", response = EventData.class, responseContainer = "List")
	public Response findGuid(@Context UriInfo info,
			@FormDataParam("file") InputStream uploadedInputStream,
			@ApiParam(value = "file: the filename containing a space separated list of run ev numbers", required = true) 
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@ApiParam(value = "stream: the stream", required = true) @FormDataParam("stream") String streamName,
			@ApiParam(value = "select: comma separated list of fields", required = true) @FormDataParam("select") @DefaultValue("runNumber,eventNumber,guid_ESD") final String keys
			)
			throws ConddbWebException {
		this.log.info("EioRestController processing request to get a list of Guids in Panda way using "+streamName+" "+fileDetail.getFileName());

		try {
//			List<String> selectlist = Arrays.asList(keys.split(","));
			List<EventData> evdata = eioDao.createListFromFile(uploadedInputStream);
			List<EventData> entitylist = eioDao.findEventsFromInputList(evdata, streamName);
			log.info("Retrieved list of events: "+entitylist.size());
			return Response.status(Response.Status.OK).entity(entitylist).build();
		} catch (Exception e) {
			String msg = "Error retrieving Guids resource ";
			throw buildException(msg + ": " + e.getMessage(), msg, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("/reguids")
	@ApiOperation(value = "Finds GUIDs by stream using a pair of run/event numbers and a stream.", notes = "Search for a list of GUIDS", response = EventData.class, responseContainer = "List")
	public Response findGuidForRunEvent(@Context UriInfo info,
			@ApiParam(value = "run: the runnumber", required = true) @QueryParam("run") final Long runnumber,
			@ApiParam(value = "event: the event number", required = true) @QueryParam("event") final Long evnumber,
			@ApiParam(value = "stream: the stream name", required = true) @QueryParam("stream") @DefaultValue("physics_Main") final String streamName,
			@ApiParam(value = "select: comma separated list of fields", required = true) @QueryParam("select") @DefaultValue("runNumber,eventNumber,guid_ESD") final String keys
			)
			throws ConddbWebException {
		this.log.info("EioRestController processing request to get a list of Guids using "+streamName+" "+runnumber+" "+evnumber);

		try {
			//List<String> selectlist = Arrays.asList(keys.split(","));
			// Dump run,ev into a stream
		    String line = runnumber+" "+evnumber;

		    InputStream istream = new ByteArrayInputStream(line.getBytes("UTF-8"));
		    List<EventData> evdata = eioDao.createListFromFile(istream);
		    List<EventData> entitylist = eioDao.findEventsGuids(evdata, streamName);
		    if (entitylist == null) {
		    	return Response.status(Response.Status.NOT_FOUND).build();
		    }
			log.debug("Retrieved list of events: "+entitylist.size());

			return Response.status(Response.Status.OK).entity(entitylist).build();
		} catch (Exception e) {
			String msg = "Error retrieving Guids resource ";
			throw buildException(msg + ": " + e.getMessage(), msg, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("/{dataset}/countev")
	@ApiOperation(value = "Finds number of events in a dataset", notes = "Count events in dataset", response = Long.class)
	public Response eventcount(@Context UriInfo info,
			@ApiParam(value = "dataset: the dataset name", required = true) @PathParam("dataset") final String dataset
			)
			throws ConddbWebException {
		this.log.info("EioRestController processing request to get events count in a dataset "+dataset);

		try {
			Long eventcount = eioDao.eventCountInDataset(dataset);
			log.debug("Retrieved event count: "+eventcount);
			if (eventcount<0) {
				return Response.status(Response.Status.NOT_FOUND).entity(eventcount).build();
			}
			return Response.status(Response.Status.OK).entity(eventcount).build();
		} catch (Exception e) {
			String msg = "Error retrieving event count for dataset "+dataset;
			throw buildException(msg + ": " + e.getMessage(), msg, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Path("/pick/{stream}/{neventmin}/{neventmax}")
	@ApiOperation(value = "Create a list of comma separated run evnum", notes = "Access table for picking run events")
	public Response pickrunev(@Context UriInfo info,
			@ApiParam(value = "stream: the stream name", required = true) 
			@PathParam("stream") @DefaultValue("physics_Main") final String streamName,
			@ApiParam(value = "number of events to pick", required = true) 
			@PathParam("neventmin") final Integer neventsmin,
			@ApiParam(value = "number of events to pick", required = true) 
			@PathParam("neventmax") final Integer neventsmax
			)
			throws ConddbWebException {
		this.log.info("EioRestController processing request to get list of run events between "+neventsmin+" and "+neventsmax+" in stream "+streamName);

		try {
			List<Map<String,Object>> entitylist = eioDao.pickevents(neventsmin,neventsmax,streamName);
			Integer nevents = (neventsmax-neventsmin);
			log.debug("Retrieved events: "+nevents);
			if (entitylist == null || entitylist.size()<=0) {
				return Response.status(Response.Status.NOT_FOUND).build();
			}
			StringBuffer buf = new StringBuffer();
			entitylist.stream().forEach(m -> buf.append(m.get("RUN_ID")+","+m.get("EVENT_ID")+","+m.get("STREAMNAME")+"\n"));
			
			InputStream in = new ByteArrayInputStream(buf.toString().getBytes());

			StreamingOutput stream = new StreamingOutput() {
				@Override
				public void write(OutputStream os) throws IOException, WebApplicationException {
					try {
						int read = 0;
						byte[] bytes = new byte[2048];

						while ((read = in.read(bytes)) != -1) {
							os.write(bytes, 0, read);
							log.debug("Copying " + read + " bytes into the output...");
						}
						os.flush();
					} catch (Exception e) {
						throw new WebApplicationException(e);
					} finally {
						log.debug("closing streams...");
						os.close();
						in.close();
					}
				}
			};
			log.debug("Send back the stream....");
			return Response.ok(stream, MediaType.APPLICATION_JSON_TYPE)
					.header("Content-Disposition", "attachment; filename=\"neventspick-" + nevents + "\".csv")
					.build();
		} catch (Exception e) {
			String msg = "Error retrieving event list of n "+(neventsmax-neventsmin)+" from stream "+streamName;
			throw buildException(msg + ": " + e.getMessage(), msg, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

}
