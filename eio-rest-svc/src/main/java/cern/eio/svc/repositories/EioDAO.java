/**
 * 
 */
package cern.eio.svc.repositories;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import cern.eio.data.views.EventData;
import cern.eio.svc.annotations.ProfileExecution;
import cern.eio.svc.exceptions.EioSvcException;


/**
 * @author aformic
 *
 */
public interface EioDAO {

	Long eventCountInDataset(String datasetname) throws EioSvcException;
	
	@ProfileExecution
	List<Map<String,Object>> findEventsGuidsFromInputFile(InputStream fileis, String streamName, List<String> keys);

	@ProfileExecution
	List<Map<String,Object>> findEventsGuidsFromInputList(List<EventData> evlist, String streamName, List<String> keys);

	@ProfileExecution
	List<EventData> findEventsFromInputList(List<EventData> evlist, String streamName);

	List<EventData> findEventsGuids(List<EventData> evlist, String streamName);

	List<Map<String,Object>> pickevents(Integer neventsmin, Integer neventsmax, String streamName);

	@ProfileExecution
	List<EventData> createListFromFile(InputStream fileis);
}
