/**
 * 
 */
package cern.eio.svc.repositories.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cern.eio.data.mappers.EventDataMapper;
import cern.eio.data.views.EventData;
import cern.eio.svc.exceptions.EioSvcException;
import cern.eio.svc.repositories.EioDAO;

/**
 * @author aformic
 *
 */
@Repository
@Transactional(readOnly = true)
public class EiDAOImpl implements EioDAO {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	private EntityManager manager;

	private JdbcTemplate jdbcTemplate;

	private static Integer FETCH_SIZE = 1000;

	@Autowired
	@Qualifier("daoDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Long eventCountInDataset(String datasetname) throws EioSvcException {
		String sql = "select COUNT_EVENTS from ATLAS_EVENTINDEX.EI_REALEVENT_DATASETS "
				+ " where PROJECT = ? and RUNNUMBER = ? and STREAMNAME = ? and PRODSTEP = ? and DATATYPE = ? and AMITAG = ?";
		String[] args = datasetname.split("\\.");
		log.debug("Splitted dataset contains n " + args.length + " fields ...");
		for (int i = 0; i < args.length; i++)
			log.debug("argument " + i + " = " + args[i]);

		List<Long> _list = jdbcTemplate.query(sql,
				new Object[] { args[0], new Long(args[1]), args[2], args[3], args[4], args[5] },
				(rs, rowNum) -> rs.getLong(1));
		log.debug("Retrieved list " + _list);
		if (_list.isEmpty()) {
			return -1L;
		} else if (_list.size() > 1) {
			throw new EioSvcException("Query to count events has given a list greater than one as result...");
		}
		return _list.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cern.eio.svc.repositories.EioDAO#findEventsFromInputList(java.util.List,
	 * java.lang.String)
	 */
	@Override
	@Transactional
	public List<EventData> findEventsFromInputList(List<EventData> evlist, String streamName) {
		log.debug("Prepare temporary table");
		jdbcTemplate.setFetchSize(FETCH_SIZE);
		int[] insertions = batchUpdate(evlist);
		log.debug("Temporary table filled...now query using stream " + streamName + " returned insertions "
				+ insertions.length);
		String sql = "SELECT RUNNUMBER, EVENTNUMBER, PROJECT, STREAMNAME, PRODSTEP, DATATYPE, "
				+ "'none' as AMITAG, GUID_ESD, GUID_AOD, GUID_RAW FROM ATLAS_EVENTINDEX.V_PANDA_EVPICK_NOAMITAG_MANY "
				+ " WHERE streamName=?";
		log.debug("Use query " + sql);
		// return jdbcTemplate.query(sql, new EventDataMapper(), new Object[] {
		// streamName });
		return jdbcTemplate.query(sql, new Object[] { streamName },
				(rs, rowNum) -> new EventData(rs.getLong(1), rs.getLong(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),
						rs.getString(10)));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cern.eio.svc.repositories.EioDAO#findEventsGuidsFromInputFile(java.lang.
	 * String)
	 */
	@Override
	@Transactional
	public List<Map<String, Object>> findEventsGuidsFromInputFile(InputStream fileis, String streamName,
			List<String> keys) {
		log.debug("Prepare temporary table");
		jdbcTemplate.setFetchSize(FETCH_SIZE);
		int[] insertions = batchUpdate(createListFromFile(fileis));
		log.debug("Temporary table filled...now query using stream " + streamName + " returned insertions "
				+ insertions.length);
		String selectfields = String.join(",", keys);
		String sql = "SELECT " + selectfields
				+ " FROM ATLAS_EVENTINDEX.V_PANDA_EVPICK_NOAMITAG_MANY WHERE streamName=?";
		log.debug("Use query " + sql);
		return jdbcTemplate.queryForList(sql, new Object[] { streamName });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cern.eio.svc.repositories.EioDAO#findEventsGuidsFromInputList(java.util.
	 * List, java.lang.String, java.util.List)
	 */
	@Override
	public List<Map<String, Object>> findEventsGuidsFromInputList(List<EventData> evlist, String streamName,
			List<String> keys) {
		log.debug("Prepare temporary table");
		jdbcTemplate.setFetchSize(FETCH_SIZE);
		int[] insertions = batchUpdate(evlist);
		log.debug("Temporary table filled...now query using stream " + streamName + " returned insertions "
				+ insertions.length);
		String selectfields = String.join(",", keys);
		String sql = "SELECT " + selectfields
				+ " FROM ATLAS_EVENTINDEX.V_PANDA_EVPICK_NOAMITAG_MANY WHERE streamName=?";
		log.debug("Use query " + sql);
		return jdbcTemplate.queryForList(sql, new Object[] { streamName });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cern.eio.svc.repositories.EioDAO#createListFromFile(java.io.InputStream)
	 */
	@Override
	public List<EventData> createListFromFile(InputStream fileis) {
		BufferedReader br = null;
		String line = "";
		// String splitBy = "\\s+";
		String splitBy = "[,\\s]+";
		List<EventData> evlist = new ArrayList<EventData>();
		try {
			InputStreamReader isr = new InputStreamReader(fileis);
			br = new BufferedReader(isr);
			while ((line = br.readLine()) != null) {
				// use spaces as separator
				if (line.isEmpty())
					continue;
				String[] events = line.split(splitBy);
				// log.debug("Run,event [run= " + events[0] + " , event=" +
				// events[1] + "]");
				Long runnum = new Long(events[0]);
				Long evnum = new Long(events[1]);
				EventData evd = new EventData(runnum, evnum, null);
				evlist.add(evd);
			}
			log.debug("temporary table filled....");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			log.debug("Some other exception occurred....");
			e.printStackTrace();
		} finally {
			log.debug("Reach finally block....");
			if (br != null) {
				try {
					log.debug("Close reader stream....");
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return evlist;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cern.eio.svc.repositories.EioDAO#findEventsGuidsFromInputFile(java.lang.
	 * String)
	 */
	@Override
	@Transactional
	public List<EventData> findEventsGuids(List<EventData> evlist, String streamName) {
		log.debug("Prepare temporary table");
		jdbcTemplate.setFetchSize(FETCH_SIZE);
		int[] insertions = batchUpdate(evlist);
		log.debug("Temporary table filled...now query using stream " + streamName + " returned insertions "
				+ insertions.length);
		// String selectfields = String.join(",", keys);
		String sql = "SELECT RUNNUMBER, EVENTNUMBER, PROJECT, STREAMNAME, PRODSTEP, DATATYPE, "
				+ "'none' as AMITAG, GUID_ESD, GUID_AOD, GUID_RAW "
				+ " FROM ATLAS_EVENTINDEX.V_PANDA_EVPICK_NOAMITAG_MANY WHERE streamName=?";
		log.debug("Use query " + sql);
		return jdbcTemplate.query(sql, new EventDataMapper(), new Object[] { streamName });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see cern.eio.svc.repositories.EioDAO#pickevents(java.lang.Integer)
	 */
	@Override
	public List<Map<String, Object>> pickevents(Integer neventsmin, Integer neventsmax, String streamName) {
		log.debug("Access run event table");
		String sql = "SELECT RUN_ID, EVENT_ID, STREAMNAME FROM atlas_eventindex.RUN_EVENT_PAIRS rep LEFT JOIN atlas_eventindex.EI_REALEVENT_DATASETS erd on erd.DATASET_ID=rep.DATASET_ID "
				+ " WHERE erd.STREAMNAME = ? and rownum<?";
		log.debug("Use query " + sql);
		return jdbcTemplate.queryForList(sql, new Object[] { streamName, neventsmax });
	}

	public int[] batchUpdate(final List<EventData> eventList) {
		String sql = "insert into ATLAS_EVENTINDEX.TMP_RUN_EVENT_PAIRS (runnumber, eventnumber) values (?, ?)";
		int[] updateCounts = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setLong(1, eventList.get(i).getRunnumber());
				ps.setLong(2, eventList.get(i).getEvent());
			}

			public int getBatchSize() {
				return eventList.size();
			}
		});
		return updateCounts;
	}
}
