/**
 * 
 */
package cern.eio.svc.exceptions;

/**
 * @author aformic
 *
 */
public class EioSvcException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9016254363207272279L;

	public EioSvcException(String string) {
		super(string);
	}

	@Override
	public String getMessage() {
		return "ConddbServiceException: " + super.getMessage();
	}

}
