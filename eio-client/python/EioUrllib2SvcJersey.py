'''
Created on Feb 26, 2015

@author: formica
'''

import sys, os, pickle, getopt,re
import json
import urllib2
import cStringIO
import os.path
import urllib
from cStringIO import StringIO
from urllib2 import Request, urlopen, URLError, HTTPError

import socks
import socksipyhandler
#from multipartform import MultiPartForm
from encode import multipart_encode
from streaminghttp import register_openers

from xml.dom import minidom
#from clint.textui import colored
from datetime import datetime

try:
    # python 3
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib import urlencode


class RequestWithMethod(urllib2.Request):
  def __init__(self, method, *args, **kwargs):
    print 'Calling constructor of RequestWithMethod ',method,' ',args
    self._method = method
    urllib2.Request.__init__(*args, **kwargs)

  def get_method(self):
    return self._method


class EIRestConnection:
    
    __baseurl = 'localhost:8080/'
    __debug = False
    __header = None
    __timeout = 100
    __username = ''
    __password = ''
    
    def setdebug(self,value):
        self.__debug = value
    
    def getBaseUrl(self):
        return self.__baseurl
    
    def setBaseUrl(self,baseurl):
        if self.__debug:
            print 'DEBUG: Setting base url ',baseurl
        self.__baseurl=baseurl
    
    def setUserPassword(self,user,password):
        self.__username = user
        self.__password = password
        
    def setSocks(self, host, port):
        self.__host = host
        self.__port = port
        print 'Setting socks proxy ', host,' ',port
        self.__opener = urllib2.build_opener(socksipyhandler.SocksiPyHandler(socks.PROXY_TYPE_SOCKS5, self.__host, self.__port))
    
    def setUrl(self, url):
        if self.__debug:
            print "DEBUG: Setting url ", url
        if 'http://' in str(url):
            #            print "DEBUG: Setting url overriding base URL: ", url
            self.__url = str(url)
        else:
            #            print "DEBUG: Setting url using baseurl ", self.__baseurl
            self.__url = self.__baseurl + url
        if self.__debug:
            print "DEBUG: URL after checking for http string: ", url
        
#        self.__curl.setopt(pycurl.URL, self.__url)
        
    def downloadData(self, filename):
        if self.__debug:
            print "DEBUG: Download data using url ", self.__url
        fp = open(filename, "wb")
        req = Request(self.__url)
        try:
            response = self.__opener.open(req)
        except HTTPError as e:
            print '====== HTTPError occurred     ======='
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            print 'Reason ',e.reason
            details = e.read()
            print 'Details: ',details
            jsondata = json.loads(details)
            print 'Extracted message: ',jsondata['internalMessage']
            print '====== End of HTTPError report ======='
            raise e
        except URLError as e:
            print '====== URLError occurred      ======='
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            print '====== End of URLError report ======='
            raise e
        else:
        # everything is fine
            fp.write(response.read())
            fp.close()
            if self.__debug:
                print 'DEBUG: Received data into output file ',filename

            return      


    def getData(self):
        if self.__debug:
            print "DEBUG: Get data using url ", self.__url
            
        req = Request(self.__url)
        req.add_header("Accept",'application/json')
        try:
            #            print 'Sending request ',req
            response = self.__opener.open(req)
        #            print 'Received response '
        except HTTPError as e:
            print '====== HTTPError occurred     ======='
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            print 'Reason ',e.reason
            details = e.read()
            print 'Details: ',details
            jsondata = json.loads(details)
            print 'Extracted message: ',jsondata['internalMessage']
            print '====== End of HTTPError report ======='
            raise e
        except URLError as e:
            print '====== URLError occurred      ======='
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            print '====== End of URLError report ======='
            raise e
        else:
        # everything is fine
        #            print '...read response...'
            data = response.read()
            #            print '...data...',data
            if self.__debug:
                print 'DEBUG: Received data', data
            if data is '':
                jsondata = {}
                jsondata['code'] = response.getcode()
                return jsondata

            jsondata = json.loads(data)
            if self.__debug:
                print 'DEBUG: JSON data ', jsondata, ' and return Code: ',response.getcode()
            objret = {}
            objret['data'] = jsondata
            objret['code'] = response.getcode()
            return objret
        
    def deleteData(self):
        if self.__debug:
            print "DEBUG: Delete data using url ", self.__url

        req = Request(self.__url)
        #####req = RequestWithMethod('DELETE',self.__url)
        req.get_method = lambda: 'DELETE'
        
        try:
            response = self.__opener.open(req)
        except HTTPError as e:
            print '====== HTTPError occurred     ======='
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            print 'Reason ',e.reason
            details = e.read()
            print 'Details: ',details
            jsondata = json.loads(details)
            print 'Extracted message: ',jsondata['internalMessage']
            print '====== End of HTTPError report ======='
            raise e
        except URLError as e:
            print '====== URLError occurred      ======='
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            print '====== End of URLError report ======='
            raise e
        else:
            # everything is fine        
            if self.__debug:
                print 'DEBUG: Retrieved data from DELETE:' , response, ' Code: ',response.getcode()
        
    def setTimeout(self,to):
        self.__timeout=to
        print('Not implemented')
#        self.__curl.setopt(self.__curl.TIMEOUT, self.__timeout)

    def setHeader(self,header):
        self.__header = header
        print('WARNING: Not implemented')

# This function should set appropriate values for POST type
    def postData(self, jsonobj):
        if self.__debug:
            print "DEBUG: post data using url ", self.__url
        post_data = jsonobj
        
        req = Request(self.__url,post_data)
        req.add_header("Content-Type",'application/json')
        req.add_header("Accept",'application/json')
        try:
            response = self.__opener.open(req)
            ### was response = urlopen(req)
        except HTTPError as e:
            print '====== HTTPError occurred     ======='
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            print 'Reason ',e.reason
            details = e.read()
            print 'Details: ',details
            jsondata = json.loads(details)
            print 'Extracted message: ',jsondata['internalMessage']
            print '====== End of HTTPError report ======='
            raise e
			
        except URLError as e:
            print '====== URLError occurred      ======='
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            print '====== End of URLError report ======='
            raise e
        else:
        # everything is fine
            data = response.read()
            if self.__debug:
                print 'DEBUG: Received data', data
            if data is '':
                return None
            jsondata = json.loads(data)
            if self.__debug:
                print 'DEBUG: JSON data ', jsondata
                    ###if isinstance(jsondata, list):
            objret = {}
            objret['data'] = jsondata
            objret['code'] = response.getcode()
            return objret
             
# This function should set appropriate values for POST type
    def postAction(self, actionparams):
        if self.__debug:
            print "DEBUG: post data action using url ", self.__url
            #print actionparams

        req = Request(self.__url,actionparams)
        req.add_header("Content-Type",'application/json')
        req.add_header("Accept",'application/json')
        try:
            ##response = urlopen(req)
            response = self.__opener.open(req)

        except HTTPError as e:
            print '====== HTTPError occurred     ======='
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            print 'Reason ',e.reason
            details = e.read()
            print 'Details: ',details
            jsondata = json.loads(details)
            print 'Extracted message: ',jsondata['internalMessage']
            print '====== End of HTTPError report ======='
            raise e
        except URLError as e:
            print '====== URLError occurred      ======='
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            print '====== End of URLError report ======='
            raise e
        else:
        # everything is fine
            data = response.read()
            if self.__debug:
                print 'DEBUG: Received data', data
            if data is '':
                return None
            jsondata = json.loads(data)
            if self.__debug:
                print 'DEBUG: JSON data ', jsondata
            objret = {}
            objret['data'] = jsondata
            objret['code'] = response.getcode()
            return objret

# This function should set appropriate values for POST type
    def postForm(self, post_data):
        if self.__debug:
            print "DEBUG: Post form using url ", self.__url
        if self.__debug:
            print post_data
            
# Start the multipart/form-data encoding of the file "DSC0001.jpg"
# "image1" is the name of the parameter, which is normally set
# via the "name" parameter of the HTML <input> tag.

# headers contains the necessary Content-Type and Content-Length
# datagen is a generator object that yields the encoded parameters
# 
#         formdata = {
#                     'file': open(post_data['file'], 'rb'),
#                     'package' : str(post_data['package']),
#                     'path': str(post_data['path']),
#                     'since': str(post_data['since']),
#                     'description': str(post_data['description'])
#                     }
        datagen, headers = multipart_encode(post_data)
        if self.__debug:
            print 'generated body and headers ...'
            print datagen
            print headers
        

# Create the Request object
        req = Request(self.__url, datagen)
        req.add_header("Accept",'application/json')
        for key in headers:
            print key
            req.add_header(key,headers[key])
        
        if self.__debug:
            print 'DEBUG: OUTGOING DATA ...'
            print req.get_data()

        try:
            response = urlopen(req)
        # SOCKS DOES NOT WORK here ....
#            response = self.__opener.open(req)
        except HTTPError as e:
            print '====== HTTPError occurred     ======='
            print 'The server couldn\'t fulfill the request.'
            print 'Error code: ', e.code
            print 'Reason ',e.reason
            details = e.read()
            print 'Details: ',details
            jsondata = json.loads(details)
            print 'Extracted message: ',jsondata['internalMessage']
            print '====== End of HTTPError report ======='
            raise e
        except URLError as e:
            print '====== URLError occurred      ======='
            print 'We failed to reach a server.'
            print 'Reason: ', e.reason
            print '====== End of URLError report ======='
            raise e
        except Exception as e:
            print '====== Generic exception occurred      ======='
            print 'method failed.'
            #e = sys.exc_info()[0]
            #            import traceback
            #print traceback.print_stack()
            print 'Reason: ', str(e)
            print '====== End of Generic exception report ======='
            raise e
        else:
        # everything is fine
            data = response.read()
            if self.__debug:
                print 'DEBUG: Received data', data
            if data is '':
                return None
            jsondata = json.loads(data)
            if self.__debug:
                print 'DEBUG: JSON data ', jsondata
            objret = {}
            objret['data'] = jsondata
            objret['code'] = response.getcode()
            return objret

    def setDefaultOptions(self):
        if self.__debug:
            print 'Set default options...none'

    def close(self):
        self.__opener.close()

    def getFormFile(self):
        return
        #return self.__curl.FORM_FILE;

    def __init__(self):
        # Register the streaming http handlers with urllib2
        #self.__opener = urllib2.build_opener()
        self.__opener = register_openers()
        self.dataresponse = "none"

   
class EIClient(object):
    '''
    classdocs
    '''
    baseurl='/api/rest/expert'
    userbaseurl='/api/rest'
    adminbaseurl='/api/rest/admin'
    __debug = False


    def getGUIDs(self,params,servicebase="/datasets/guids"):
        if self.__debug:
            print 'DEBUG: select guids using file in input '
            print params
        url = (self.userbaseurl + servicebase)
        self.__curl.setUrl(url)
        formdata = {
            'file': open(params['file'], 'rb'),
            'stream' : str(params['stream']),
            'select' : str(params['select'])
        }
        return self.__curl.postForm(formdata)

    def getGUIDsFromRunEv(self,params,servicebase="/datasets/reguids"):
        if self.__debug:
            print 'DEBUG: select guids using input run event params'
            print params
        args = { 'run' : params['run'], 'event' : params['event'], 'stream' : params['stream']}
        if params['select'] is not None:
            args['select'] = params['select']
        pairs = urllib.urlencode(args)
        url = (self.userbaseurl + servicebase)
        urlquoted = urllib.quote_plus(url,safe=':/._')
        self.__curl.setUrl(urlquoted+'?'+pairs)
        return self.__curl.postAction(pairs)

    def getEvcount(self,params,servicebase="/datasets"):
        if self.__debug:
            print 'DEBUG: Add calibration file using input '
            print params
        url = (self.userbaseurl + servicebase + '/' + params['dataset']+'/countev')
        urlquoted = urllib.quote_plus(url,safe=':/._&?')
        self.__curl.setUrl(urlquoted)
    
        return self.__curl.getData()

    def setUserPassword(self,user,passwd):
#        self.__curl.setUserPassword(user, passwd)
        print 'WARNING: Not implemented'
           
    def close(self):
        print 'WARNING: Not implemented ??'
#        self.__curl.close()

    def setdebug(self,value):
        self.__debug = value
        self.__curl.setdebug(value)

    def getFormFile(self):
        print 'WARNING: Not implemented'
        return self.__curl.getFormFile()
    
    def __init__(self, servicepath, usesocks=False):
        '''
        Constructor
        '''
        if self.__debug:
            print 'DEBUG: Access to EI-Oracle via REST services'
        self.__servicepath = servicepath
        self.__curl = EIRestConnection()
        self.__curl.setBaseUrl('http://'+self.__servicepath)
        if usesocks == True:
            self.__curl.setSocks('localhost', 3129)


class EIUtils(object):
    '''
    classdocs
    '''
    __debug = False
    __restserver = {}
    
    def __init__(self, restserver):
        '''
        Constructor
        '''
        if self.__debug:
            print 'DEBUG: Function utils for command lines'
        self.__restserver = restserver
 
    def printmsg(self,msg,color):
        try:
          from clint.textui import colored
          if color == 'cyan':
          	print colored.cyan(msg)
          elif color == 'blue':
            print colored.blue(msg)
          elif color == 'red':
            print colored.red(msg)
          elif color == 'green':
            print colored.green(msg)
          elif color == 'yellow':
            print colored.yellow(msg)
          else:
          	print colored.cyan(msg)
        except:
          print msg

       
    def printItems(self, data):
    # Assume that data is a list of items
        for anobj in data:
            print anobj
            href = anobj['href']
            url = {}
            url['href'] = href
            if self.__debug:
               print 'DEBUG: Use url link ',url
            obj = self.__restserver.getlink(url)
            print obj

# load items from link
    def loadItems(self, data):
        # Check if data is a single object
        if 'href' in data:
            href = data['href']
            url = {}
            url['href'] = href
            if self.__debug:
                print 'DEBUG: Use url link ',url
            obj = self.__restserver.getlink(url)
            if self.__debug:
                print 'DEBUG: Retrieved object from link  ',obj
            
            return obj
            

