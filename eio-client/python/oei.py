#!/usr/bin/env python
# encoding: utf-8
'''
cliadmin.CalibAdmin -- shortdesc

cliadmin.CalibAdmin is a description

It defines classes_and_methods

@author:     formica

@copyright:  2015 CEA. All rights reserved.

@license:    license

@contact:    andrea.formica@cea.fr
@deffield    updated: Updated
'''

import sys, os, pickle, getopt,re
import json
import cStringIO
import os.path

from xml.dom import minidom
##from clint.textui import colored
from datetime import datetime

from EioUrllib2SvcJersey import EIClient,EIUtils

class EioDBDriver():
    def __init__(self):
    # process command line options
        try:
            self.restserver = {}
            self.resttools = {}
            self._command = sys.argv[0]
            self.useSocks = False
            self.t0 = 0
            self.debug = False
            self.stream = 'physics_Main'
            self.evfile = 'none'
            self.select = 'runNumber,eventNumber,guid_ESD'
            self.expand = 'false'
            self.gtype = None
            self.snapshot = '2050-01-01T00:00:00+02:00'
            self.jsondump=False
            self.dump=False
            self.user='none'
            self.passwd='none'
            self.outfilename=''
            self.urlsvc='localhost:8080/eio'
            longopts=['help','socks','out=','jsondump','debug','url=','t0desc=','stream=','evfile=','snapshot=','select=','expand=','gtype=','user=','pass=']
            opts,args=getopt.getopt(sys.argv[1:],'',longopts)
        #print opts, args
        except getopt.GetoptError,e:
#            print colored.red(e)
            print e
            self.usage()
            sys.exit(-1)
        self.procopts(opts,args)
        self.execute()

    def usage(self):
        print
        print "usage: oei {<options>} <action> {<args>}"
        print "Retrieve informations from event index"
        print " "
        print "<Action> determines which rest method to call"
        print "Subcommands are:"
        print " - guids [runnum] [evnum] "
        print "      You can either use optional arguments runnum evnum or declare --evfile=<filename> "
        print "      with the list of runnum evnum paris that you want to search for."
        print "        ex: you can use as an example https://gallas.web.cern.ch/gallas/RunEventLists/Buttinger_picklist_full.txt "
        print "          oei --stream=physics_Main --evfile=Buttinger_picklist_full.txt guids "
        print "        ex: provide runnum and evnum "
        print "          oei --stream=physics_Main guids runnum evnum"
        print "    Gives a list of : runnumber eventnumber GUID for every entry in the input file for the selected stream "
        print " "
        print " - TAG <package-name> <global tag name>"
        print "        ex: MyNewPkg MyNewPkg-00-01 : creates new global tag and associate all tags (files) found in package MyNewPkg."
        print "     Creates new global tag <global tag name> associated to all files in package <package-name>."
        print " "
        print " - LOCK <global tag name> <lockstatus> [locked|unlocked]"
        print "        ex: MyNewPkg-00-01 locked : lock the global tag and change its snapshot time to now."
        print "     Lock or unlock existing global tag <global tag name>."
        print " "
        print " - LS  <global tag name> <filter file name> [pattern | *(DEFAULT)] "
        print "        ex: MyNewPkg-00-01 myfile : retrieve list of files containing 'myfile' string in global tag MyNewPkg-00-01."
        print "    List files under a given global tag, filtering by file name."
        print " "
        print " - SHOW  <folder path> <tag-name> [tag-name defaults to _HEAD_00]"
        print "        ex: /MY/FOLDER/PATH : retrieve list of files in the folder showing their insertion time and datasize."
        print "    List files under a given folder (and tag)."
        print " "
        print " - DUMP  hash [hash of a file can be retrieved using LS and SHOW commands]"
        print "        ex: anaash : dump the file in local directory."
        print "    Dump the file in the local directory."
        print " "
        print " - COLLECT  <global tag name> <ASG global tag>"
        print "        ex: MyNewPkg-00-01 ASG-00-01: Dump the full directory structure for global tag in server file system."
        print "    The purpose is to trigger the copy to afs of all calibration files under a global tag."
        print " "
        print "Options: "
        print "  --socks activate socks proxy on localhost 3129 "
        print "  --out={filename} activate dump on filename "
        print "  --debug activate debugging output "
        print "  --jsondump activate a dump of output lines in json format "
        print "  --trace [on|off]: trace associations (ex: globaltag ->* tags or tag ->* iovs "
        print "  --url [localhost:8080/physconddb]: use a specific server "
        print "  --snapshot={the snapshot time for global tag, default is 2050-01-01T00:00:00+02:00} "
        print "  --stream={selection of the stream: physics_Main is the default} "
        print "  --t0desc={description string for t0, can be used for payload file name} "
        print "  --evfile={run event file name} "
        print "  --gtype={ESD|AOD|...} "


    def procopts(self,opts,args):
        "Process the command line parameters"
        for o,a in opts:
            #           print 'Analyse options ' + o + ' ' + a
            if (o=='--help'):
                self.usage()
                sys.exit(0)
            if (o=='--socks'):
                self.useSocks=True
            if (o=='--out'):
                self.dump=True
                self.outfilename=a
            if (o=='--debug'):
                self.debug=True
            if (o=='--jsondump'):
                self.jsondump=True
            if (o=='--url'):
                self.urlsvc=a
            if (o=='--snapshot'):
                self.snapshot=a
            if (o=='--stream'):
                self.stream=a
            if (o=='--t0desc'):
                self.t0desc=a
            if (o=='--evfile'):
                self.evfile=a
            if (o=='--user'):
                self.user=a
            if (o=='--select'):
                self.select=a
            if (o=='--expand'):
                self.expand=a
            if (o=='--pass'):
                self.passwd=a
            if (o=='--gtype'):
                self.gtype=a
                    
        if (len(args)<1):
            raise getopt.GetoptError("Insufficient arguments - need at least 2, or try --help")
        self.action=args[0].upper()
        self.args=args[1:]


    def printmsg(self,msg,color):
        try:
          from clint.textui import colored
          if color == 'cyan':
          	print colored.cyan(msg)
          elif color == 'blue':
            print colored.blue(msg)
          elif color == 'red':
            print colored.red(msg)
          elif color == 'green':
            print colored.green(msg)
          elif color == 'yellow':
            print colored.yellow(msg)
          else:
          	print colored.cyan(msg)
        except:
            print msg

    def printjson(self,data,params):
        if isinstance(data,list):
            for entry in data:
                msg = ''
                for key in params:
                    msg += ('%s ') % str(entry[key])
                self.printmsg(msg,'blue')
        else:
            self.printmsg('input data is not list...','red')

######## Utility functions

    def execute(self):
        #print colored.blue(('Execute the command for action %s and arguments : %s ' ) % (self.action, str(self.args)))
        msg = ('Execute the command for action %s and arguments : %s ' ) % (self.action, str(self.args))
        self.printmsg(msg,'blue')
        start = datetime.now()
        self.restserver = EIClient(self.urlsvc, self.useSocks)
        self.resttools = EIUtils(self.restserver)
        
        if self.debug:
            self.restserver.setdebug(True)
        
        if self.dump:
            outfile = open(self.outfilename,"w")
        _dict = {}
        dictarr = []
        dictkeys = []
        dictvalues = []
        params = {}
        
        if (self.action=='GUID'):
            try:
                eiargs=self.args
                msg = '>>> Call method %s using arguments %s ' % (self.action,eiargs)
                self.printmsg(msg,'cyan')
                params={}
                if self.gtype is not None:
                    self.select = re.sub(r"_ESD", "_"+self.gtype, self.select.upper())
                params['stream'] = self.stream
                params['select'] = self.select.upper()
                if self.debug:
                    msg = 'Using select expression %s ' % self.select.upper()
                    self.printmsg(msg,'green')

                keys = self.select.upper().split(',')
                response = {}
                if self.evfile != 'none':
                # Run Event list file is declared, ignore all other arguments
                    params['file'] = self.evfile
                    response = self.restserver.getGUIDs(params)
                else :
                # Run Event are taken from arguments in command line
                    if len(eiargs) > 1:
                        params['run'] = eiargs[0]
                        params['event'] = eiargs[1]
                    response = self.restserver.getGUIDsFromRunEv(params)
        
                if self.debug:
                    msg = 'Retrieved data : %s ' % response
                    self.printmsg(msg,'green')
                self.printjson(response['data'],keys)
                return 0
                    
            except Exception, e:
                sys.exit("ERROR: failed %s" % (str(e)))
                raise e
        
        elif (self.action=='EVCOUNT'):
            try:
                eiargs=self.args
                msg = '>>> Call method %s using arguments %s ' % (self.action,eiargs)
                self.printmsg(msg,'cyan')
                params={}
                params['dataset']=eiargs[0]
                response = self.restserver.getEvcount(params)
                if self.debug:
                    msg = 'Retrieved data : %s ' % response
                    self.printmsg(msg,'green')
                msg = ('Dataset %s contains %d events') % (eiargs[0],response['data'])
                self.printmsg(msg,'blue')
                return 0
            
            except Exception, e:
                sys.exit("ERROR: failed %s" % (str(e)))
                raise e

        else:
            msg = ('ERROR: Command %s not recognized, type -h for help') % self.action
            self.printmsg(msg,'red')                
            return -1



if __name__ == '__main__':
    EioDBDriver()

