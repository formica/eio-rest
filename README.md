#       EIO REST api

#### Author: A.Formica
##### Date of last development period: 2016/01/18
```
   Copyright (C) 2015  A.Formica

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
## Description
Test project for the EIO rest services.
The REST services will be listed via swagger.
A client python library will be also included.

## Build instructions
Execute the following commands from project ROOT directory, in this case eio-rest.
- To compile the code: `<project-root>/$ mvn clean compile `
- To package the code: `<project-root>/$ mvn package `
- To install the compiled jars into the user maven repository: `<project-root>/$ mvn install `. The installation 
step uses the directory `~/.m2/...`. 

The previous steps can also be included in the same line
- `<project-root>/$ mvn clean install`
During this phase tests are also executed (the code is contained in `<package>/src/test/...`)

The products of the build are store in the `target` directory of every sub-module.

In order to avoid tests to be executed you can run the commands using a maven option like :
```
mvn -Dmaven.test.skip=true     
```   
test are not executed and not compiled
or
```
mvn -DskipTests=true
```
test are not executed



## Deployment instructions
The application is designed to support for the moment the following database platforms:
- `Oracle database`
Additional databases can be easily added, this will be explained later.

The application is designed to be deployed in several web containers:
- `Jetty, Tomcat7`

In order to choose dynamically in which way the application can be deployed we make usage of spring profiles.
The profiles are defined essentially inside :
- `<project-root>/eio-rest-svc/src/main/resources/spring/db/dao-datasources-context.xml` file.
Some specific properties files are associated to every profile:
- `<project-root>/eio-rest-svc/src/main/resources/spring/sql/*.properties` 

When deploying the application you can select the profiles either by uncommenting the following:
```
<!-- CAREFUL : this should be uncommented for pre-setting the spring profiles
     This is useful in particular when deploying into production system 
     <context-param> 
            <param-name>spring.profiles.active</param-name>
            <param-value>prod,oracle</param-value>
     </context-param> 
-->
```
which you can find inside the file `<project-root>/eio-rest-web/src/main/webapp/WEB-INF/web.xml`, or also via command line:
```
mvn -Dspring.profiles.active=jetty,oracle jetty:run
```

Below we mention specific instructions for Tomcat and Jetty deployment.

###   Tomcat7
Go to the tomcat installation directory to start tomcat server. In general
this is defined as `CATALINA_HOME`.
Set the profiles that you want to use before starting tomcat.

`<$CATALINA_HOME>/$ export CATALINA_OPTS="-Dspring.profiles.active=prod,oracle"`
This can be avoided if the `web.xml` file has been modified as explained above.     
Start tomcat server:
`<$CATALINA_HOME>/$ ./bin/catalina.sh start `
   
Go to project ROOT directory and choose the web module to install:
```
   <project-root>/$ cd eio-rest-web
   <project-root>/eio-rest-web$ mvn tomcat7:deploy  
```   
or
```
   <project-root>/eio-rest-web$ mvn tomcat7:redeploy  
```   
   
This command suppose that tomcat is running under: `http://localhost:8080`
   
### Jetty
Jetty is a small server embedded within the application. Single modules can be deployed in Jetty by using the following command:
```
   <project-root>/$ cd eio-rest-web
   <project-root>/eio-rest-web$ mvn -Dspring.profiles.active=jetty,oracle jetty:run
```   

## Testing the service
To be done

You can try the service using
`curl -X POST -F "stream=physics_Main" -F "file=@./full_ev_list.txt" http://localhost:8080/eio/api/rest/datasets/guids`

where the file full_ev_list.txt can be download from 
`https://gallas.web.cern.ch/gallas/RunEventLists/Buttinger_picklist_full.txt`
   
