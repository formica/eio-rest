/**
 * 
 */
package cern.eio.data.views;

import java.io.Serializable;

/**
 * @author aformic
 *
 */
public class EventData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -276965984673988634L;
	private Long runnumber;
	private Long event;
	private String project;
	private String streamName;
	private String prodStep;
	private String dataType;
	private String amiTag;
	private String guidEsd;
	private String guidAod;
	private String guidRaw;
	
	
	public EventData() {
		super();
	}


	public EventData(Long runnumber, Long event, String guid) {
		super();
		this.runnumber = runnumber;
		this.event = event;
		this.guidEsd = guid;
	}


	public EventData(Long runnumber, Long event, String project, String streamName, String prodStep, String dataType,
			String amiTag, String guidEsd, String guidAod, String guidRaw) {
		super();
		this.runnumber = runnumber;
		this.event = event;
		this.project = project;
		this.streamName = streamName;
		this.prodStep = prodStep;
		this.dataType = dataType;
		this.amiTag = amiTag;
		this.guidEsd = guidEsd;
		this.guidAod = guidAod;
		this.guidRaw = guidRaw;
	}


	/**
	 * @return the runnumber
	 */
	public Long getRunnumber() {
		return runnumber;
	}


	/**
	 * @param runnumber the runnumber to set
	 */
	public void setRunnumber(Long runnumber) {
		this.runnumber = runnumber;
	}


	/**
	 * @return the event
	 */
	public Long getEvent() {
		return event;
	}


	/**
	 * @param event the event to set
	 */
	public void setEvent(Long event) {
		this.event = event;
	}


	/**
	 * @return the guid
	 */
	public String getGuidEsd() {
		return guidEsd;
	}


	/**
	 * @param guid the guid to set
	 */
	public void setGuidEsd(String guid) {
		this.guidEsd = guid;
	}


	/**
	 * @return the project
	 */
	public String getProject() {
		return project;
	}


	/**
	 * @param project the project to set
	 */
	public void setProject(String project) {
		this.project = project;
	}


	/**
	 * @return the streamName
	 */
	public String getStreamName() {
		return streamName;
	}


	/**
	 * @param streamName the streamName to set
	 */
	public void setStreamName(String streamName) {
		this.streamName = streamName;
	}


	/**
	 * @return the prodStep
	 */
	public String getProdStep() {
		return prodStep;
	}


	/**
	 * @param prodStep the prodStep to set
	 */
	public void setProdStep(String prodStep) {
		this.prodStep = prodStep;
	}


	/**
	 * @return the dataType
	 */
	public String getDataType() {
		return dataType;
	}


	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}


	/**
	 * @return the amiTag
	 */
	public String getAmiTag() {
		return amiTag;
	}


	/**
	 * @param amiTag the amiTag to set
	 */
	public void setAmiTag(String amiTag) {
		this.amiTag = amiTag;
	}


	/**
	 * @return the guidAod
	 */
	public String getGuidAod() {
		return guidAod;
	}


	/**
	 * @param guidAod the guidAod to set
	 */
	public void setGuidAod(String guidAod) {
		this.guidAod = guidAod;
	}


	/**
	 * @return the guidRaw
	 */
	public String getGuidRaw() {
		return guidRaw;
	}


	/**
	 * @param guidRaw the guidRaw to set
	 */
	public void setGuidRaw(String guidRaw) {
		this.guidRaw = guidRaw;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((event == null) ? 0 : event.hashCode());
		result = prime * result + ((guidEsd == null) ? 0 : guidEsd.hashCode());
		result = prime * result + ((runnumber == null) ? 0 : runnumber.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventData other = (EventData) obj;
		if (event == null) {
			if (other.event != null)
				return false;
		} else if (!event.equals(other.event))
			return false;
		if (guidEsd == null) {
			if (other.guidEsd != null)
				return false;
		} else if (!guidEsd.equals(other.guidEsd))
			return false;
		if (runnumber == null) {
			if (other.runnumber != null)
				return false;
		} else if (!runnumber.equals(other.runnumber))
			return false;
		return true;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EventData [runnumber=" + runnumber + ", event=" + event + ", guid=" + guidEsd + "]";
	}
	
}
