/**
 * 
 * This file is part of PhysCondDB.
 *
 *   PhysCondDB is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   PhysCondDB is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with PhysCondDB.  If not, see <http://www.gnu.org/licenses/>.
 **/
package cern.eio.data.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import cern.eio.data.views.EventData;


/**
 * @author formica
 *
 */
public class EventDataMapper implements RowMapper<EventData> {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	/**
	 * Use fields from query :  runNumber,eventNumber,guid_ESD
	 */
	@Override
	public EventData mapRow(ResultSet rs, int rownum) throws SQLException {
		EventData nt = new EventData();
		nt.setRunnumber(rs.getLong("runNumber"));
		nt.setEvent(rs.getLong("eventNumber"));
		nt.setProject(rs.getString("PROJECT"));
		nt.setProdStep(rs.getString("PRODSTEP"));
		nt.setAmiTag(rs.getString("AMITAG"));
		nt.setDataType(rs.getString("DATATYPE"));
		nt.setStreamName(rs.getString("STREAMNAME"));
		nt.setGuidEsd(rs.getString("GUID_ESD"));
		nt.setGuidAod(rs.getString("GUID_AOD"));
		nt.setGuidRaw(rs.getString("GUID_RAW"));
		log.debug("reading row "+rownum+" and create event data "+nt);

		return nt;
	}

}
