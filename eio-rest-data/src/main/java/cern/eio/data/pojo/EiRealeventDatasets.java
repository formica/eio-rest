package cern.eio.data.pojo;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import af.cea.web.data.AfEntity;

/**
 * EiRealeventDatasets generated by hbm2java
 */
@Entity
@Table(name="EI_REALEVENT_DATASETS"
    ,schema="ATLAS_EVENTINDEX"
    , uniqueConstraints = @UniqueConstraint(columnNames={"RUNNUMBER", "PROJECT", "STREAMNAME", "PRODSTEP", "DATATYPE", "AMITAG"}) 
)
public class EiRealeventDatasets extends AfEntity implements java.io.Serializable {


     private long datasetId;
     private String project;
     private long runnumber;
     private String streamname;
     private String prodstep;
     private String datatype;
     private String amitag;
     private String guidType0;
     private String guidType1;
     private String guidType2;
     private Date insertTstamp;
     private Long countEvents;
     private Integer statusGuidUpload;
     private Integer triggerDatasetId;
     private Long uniqDuplEvents;
     private Long numDuplicates;
     private Long amiCount;
     private Date amiDate;
     private Byte latestRank;
     private Date removedFromEiTstamp;
     private Set<AuxStatsLumiblock> auxStatsLumiblocks = new HashSet<AuxStatsLumiblock>(0);

     private List<EiRealevents> events = new ArrayList<EiRealevents>();

    public EiRealeventDatasets() {
    }

	
    public EiRealeventDatasets(long datasetId, String project, long runnumber, String streamname, String prodstep, String datatype, String amitag) {
        this.datasetId = datasetId;
        this.project = project;
        this.runnumber = runnumber;
        this.streamname = streamname;
        this.prodstep = prodstep;
        this.datatype = datatype;
        this.amitag = amitag;
    }
    public EiRealeventDatasets(long datasetId, String project, long runnumber, String streamname, String prodstep, String datatype, String amitag, String guidType0, String guidType1, String guidType2, Date insertTstamp, Long countEvents, Integer statusGuidUpload, Integer triggerDatasetId, Long uniqDuplEvents, Long numDuplicates, Long amiCount, Date amiDate, Byte latestRank, Date removedFromEiTstamp, Set<AuxStatsLumiblock> auxStatsLumiblocks) {
       this.datasetId = datasetId;
       this.project = project;
       this.runnumber = runnumber;
       this.streamname = streamname;
       this.prodstep = prodstep;
       this.datatype = datatype;
       this.amitag = amitag;
       this.guidType0 = guidType0;
       this.guidType1 = guidType1;
       this.guidType2 = guidType2;
       this.insertTstamp = insertTstamp;
       this.countEvents = countEvents;
       this.statusGuidUpload = statusGuidUpload;
       this.triggerDatasetId = triggerDatasetId;
       this.uniqDuplEvents = uniqDuplEvents;
       this.numDuplicates = numDuplicates;
       this.amiCount = amiCount;
       this.amiDate = amiDate;
       this.latestRank = latestRank;
       this.removedFromEiTstamp = removedFromEiTstamp;
       this.auxStatsLumiblocks = auxStatsLumiblocks;
    }
   
     @Id 
    
    @Column(name="DATASET_ID", unique=true, nullable=false, precision=10, scale=0)
    public long getDatasetId() {
        return this.datasetId;
    }
    
    public void setDatasetId(long datasetId) {
        this.datasetId = datasetId;
    }
    
    @Column(name="PROJECT", nullable=false, length=32)
    public String getProject() {
        return this.project;
    }
    
    public void setProject(String project) {
        this.project = project;
    }
    
    @Column(name="RUNNUMBER", nullable=false, precision=10, scale=0)
    public long getRunnumber() {
        return this.runnumber;
    }
    
    public void setRunnumber(long runnumber) {
        this.runnumber = runnumber;
    }
    
    @Column(name="STREAMNAME", nullable=false, length=50)
    public String getStreamname() {
        return this.streamname;
    }
    
    public void setStreamname(String streamname) {
        this.streamname = streamname;
    }
    
    @Column(name="PRODSTEP", nullable=false, length=15)
    public String getProdstep() {
        return this.prodstep;
    }
    
    public void setProdstep(String prodstep) {
        this.prodstep = prodstep;
    }
    
    @Column(name="DATATYPE", nullable=false, length=40)
    public String getDatatype() {
        return this.datatype;
    }
    
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }
    
    @Column(name="AMITAG", nullable=false, length=100)
    public String getAmitag() {
        return this.amitag;
    }
    
    public void setAmitag(String amitag) {
        this.amitag = amitag;
    }
    
    @Column(name="GUID_TYPE0", length=30)
    public String getGuidType0() {
        return this.guidType0;
    }
    
    public void setGuidType0(String guidType0) {
        this.guidType0 = guidType0;
    }
    
    @Column(name="GUID_TYPE1", length=30)
    public String getGuidType1() {
        return this.guidType1;
    }
    
    public void setGuidType1(String guidType1) {
        this.guidType1 = guidType1;
    }
    
    @Column(name="GUID_TYPE2", length=30)
    public String getGuidType2() {
        return this.guidType2;
    }
    
    public void setGuidType2(String guidType2) {
        this.guidType2 = guidType2;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="INSERT_TSTAMP", length=7)
    public Date getInsertTstamp() {
        return this.insertTstamp;
    }
    
    public void setInsertTstamp(Date insertTstamp) {
        this.insertTstamp = insertTstamp;
    }
    
    @Column(name="COUNT_EVENTS", precision=10, scale=0)
    public Long getCountEvents() {
        return this.countEvents;
    }
    
    public void setCountEvents(Long countEvents) {
        this.countEvents = countEvents;
    }
    
    @Column(name="STATUS_GUID_UPLOAD", precision=5, scale=0)
    public Integer getStatusGuidUpload() {
        return this.statusGuidUpload;
    }
    
    public void setStatusGuidUpload(Integer statusGuidUpload) {
        this.statusGuidUpload = statusGuidUpload;
    }
    
    @Column(name="TRIGGER_DATASET_ID", precision=5, scale=0)
    public Integer getTriggerDatasetId() {
        return this.triggerDatasetId;
    }
    
    public void setTriggerDatasetId(Integer triggerDatasetId) {
        this.triggerDatasetId = triggerDatasetId;
    }
    
    @Column(name="UNIQ_DUPL_EVENTS", precision=10, scale=0)
    public Long getUniqDuplEvents() {
        return this.uniqDuplEvents;
    }
    
    public void setUniqDuplEvents(Long uniqDuplEvents) {
        this.uniqDuplEvents = uniqDuplEvents;
    }
    
    @Column(name="NUM_DUPLICATES", precision=10, scale=0)
    public Long getNumDuplicates() {
        return this.numDuplicates;
    }
    
    public void setNumDuplicates(Long numDuplicates) {
        this.numDuplicates = numDuplicates;
    }
    
    @Column(name="AMI_COUNT", precision=10, scale=0)
    public Long getAmiCount() {
        return this.amiCount;
    }
    
    public void setAmiCount(Long amiCount) {
        this.amiCount = amiCount;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="AMI_DATE", length=7)
    public Date getAmiDate() {
        return this.amiDate;
    }
    
    public void setAmiDate(Date amiDate) {
        this.amiDate = amiDate;
    }
    
    @Column(name="LATEST_RANK", precision=2, scale=0)
    public Byte getLatestRank() {
        return this.latestRank;
    }
    
    public void setLatestRank(Byte latestRank) {
        this.latestRank = latestRank;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="REMOVED_FROM_EI_TSTAMP", length=7)
    public Date getRemovedFromEiTstamp() {
        return this.removedFromEiTstamp;
    }
    
    public void setRemovedFromEiTstamp(Date removedFromEiTstamp) {
        this.removedFromEiTstamp = removedFromEiTstamp;
    }
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="eiRealeventDatasets")
    public Set<AuxStatsLumiblock> getAuxStatsLumiblocks() {
        return this.auxStatsLumiblocks;
    }
    
    public void setAuxStatsLumiblocks(Set<AuxStatsLumiblock> auxStatsLumiblocks) {
        this.auxStatsLumiblocks = auxStatsLumiblocks;
    }


    @OneToMany
    @JoinColumn(name="DATASET_ID")
    public List<EiRealevents> getEvents() {
		return events;
	}


	public void setEvents(List<EiRealevents> events) {
		this.events = events;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amitag == null) ? 0 : amitag.hashCode());
		result = prime * result + ((auxStatsLumiblocks == null) ? 0 : auxStatsLumiblocks.hashCode());
		result = prime * result + ((countEvents == null) ? 0 : countEvents.hashCode());
		result = prime * result + (int) (datasetId ^ (datasetId >>> 32));
		result = prime * result + ((prodstep == null) ? 0 : prodstep.hashCode());
		result = prime * result + ((project == null) ? 0 : project.hashCode());
		result = prime * result + (int) (runnumber ^ (runnumber >>> 32));
		result = prime * result + ((streamname == null) ? 0 : streamname.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EiRealeventDatasets other = (EiRealeventDatasets) obj;
		if (amitag == null) {
			if (other.amitag != null)
				return false;
		} else if (!amitag.equals(other.amitag))
			return false;
		if (auxStatsLumiblocks == null) {
			if (other.auxStatsLumiblocks != null)
				return false;
		} else if (!auxStatsLumiblocks.equals(other.auxStatsLumiblocks))
			return false;
		if (countEvents == null) {
			if (other.countEvents != null)
				return false;
		} else if (!countEvents.equals(other.countEvents))
			return false;
		if (datasetId != other.datasetId)
			return false;
		if (prodstep == null) {
			if (other.prodstep != null)
				return false;
		} else if (!prodstep.equals(other.prodstep))
			return false;
		if (project == null) {
			if (other.project != null)
				return false;
		} else if (!project.equals(other.project))
			return false;
		if (runnumber != other.runnumber)
			return false;
		if (streamname == null) {
			if (other.streamname != null)
				return false;
		} else if (!streamname.equals(other.streamname))
			return false;
		return true;
	}

}


