package cern.eio.data.pojo;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * EiRealeventsId generated by hbm2java
 */
@Embeddable
public class EiRealeventsId  implements java.io.Serializable {


     private long datasetId;
     private long eventnumber;

    public EiRealeventsId() {
    }

    public EiRealeventsId(long datasetId, long eventnumber) {
       this.datasetId = datasetId;
       this.eventnumber = eventnumber;
    }
   

    @Column(name="DATASET_ID", nullable=false, precision=10, scale=0)
    public long getDatasetId() {
        return this.datasetId;
    }
    
    public void setDatasetId(long datasetId) {
        this.datasetId = datasetId;
    }

    @Column(name="EVENTNUMBER", nullable=false, precision=10, scale=0)
    public long getEventnumber() {
        return this.eventnumber;
    }
    
    public void setEventnumber(long eventnumber) {
        this.eventnumber = eventnumber;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof EiRealeventsId) ) return false;
		 EiRealeventsId castOther = ( EiRealeventsId ) other; 
         
		 return (this.getDatasetId()==castOther.getDatasetId())
 && (this.getEventnumber()==castOther.getEventnumber());
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + (int) this.getDatasetId();
         result = 37 * result + (int) this.getEventnumber();
         return result;
   }   


}


