package cern.eio.data.pojo;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import java.util.Arrays;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * EiRealeventsDuplId generated by hbm2java
 */
@Embeddable
public class EiRealeventsDuplId  implements java.io.Serializable {


     private long datasetId;
     private long eventnumber;
     private Long lumiblockn;
     private Long bunchid;
     private byte[] guid0;
     private byte[] guid1;
     private byte[] guid2;
     private String guid0Char;
     private String guid1Char;
     private String guid2Char;

    public EiRealeventsDuplId() {
    }

	
    public EiRealeventsDuplId(long datasetId, long eventnumber) {
        this.datasetId = datasetId;
        this.eventnumber = eventnumber;
    }
    public EiRealeventsDuplId(long datasetId, long eventnumber, Long lumiblockn, Long bunchid, byte[] guid0, byte[] guid1, byte[] guid2, String guid0Char, String guid1Char, String guid2Char) {
       this.datasetId = datasetId;
       this.eventnumber = eventnumber;
       this.lumiblockn = lumiblockn;
       this.bunchid = bunchid;
       this.guid0 = guid0;
       this.guid1 = guid1;
       this.guid2 = guid2;
       this.guid0Char = guid0Char;
       this.guid1Char = guid1Char;
       this.guid2Char = guid2Char;
    }
   

    @Column(name="DATASET_ID", nullable=false, precision=10, scale=0)
    public long getDatasetId() {
        return this.datasetId;
    }
    
    public void setDatasetId(long datasetId) {
        this.datasetId = datasetId;
    }

    @Column(name="EVENTNUMBER", nullable=false, precision=10, scale=0)
    public long getEventnumber() {
        return this.eventnumber;
    }
    
    public void setEventnumber(long eventnumber) {
        this.eventnumber = eventnumber;
    }

    @Column(name="LUMIBLOCKN", precision=10, scale=0)
    public Long getLumiblockn() {
        return this.lumiblockn;
    }
    
    public void setLumiblockn(Long lumiblockn) {
        this.lumiblockn = lumiblockn;
    }

    @Column(name="BUNCHID", precision=10, scale=0)
    public Long getBunchid() {
        return this.bunchid;
    }
    
    public void setBunchid(Long bunchid) {
        this.bunchid = bunchid;
    }

    @Column(name="GUID0")
    public byte[] getGuid0() {
        return this.guid0;
    }
    
    public void setGuid0(byte[] guid0) {
        this.guid0 = guid0;
    }

    @Column(name="GUID1")
    public byte[] getGuid1() {
        return this.guid1;
    }
    
    public void setGuid1(byte[] guid1) {
        this.guid1 = guid1;
    }

    @Column(name="GUID2")
    public byte[] getGuid2() {
        return this.guid2;
    }
    
    public void setGuid2(byte[] guid2) {
        this.guid2 = guid2;
    }

    @Column(name="GUID0_CHAR", length=36)
    public String getGuid0Char() {
        return this.guid0Char;
    }
    
    public void setGuid0Char(String guid0Char) {
        this.guid0Char = guid0Char;
    }

    @Column(name="GUID1_CHAR", length=36)
    public String getGuid1Char() {
        return this.guid1Char;
    }
    
    public void setGuid1Char(String guid1Char) {
        this.guid1Char = guid1Char;
    }

    @Column(name="GUID2_CHAR", length=36)
    public String getGuid2Char() {
        return this.guid2Char;
    }
    
    public void setGuid2Char(String guid2Char) {
        this.guid2Char = guid2Char;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof EiRealeventsDuplId) ) return false;
		 EiRealeventsDuplId castOther = ( EiRealeventsDuplId ) other; 
         
		 return (this.getDatasetId()==castOther.getDatasetId())
 && (this.getEventnumber()==castOther.getEventnumber())
 && ( (this.getLumiblockn()==castOther.getLumiblockn()) || ( this.getLumiblockn()!=null && castOther.getLumiblockn()!=null && this.getLumiblockn().equals(castOther.getLumiblockn()) ) )
 && ( (this.getBunchid()==castOther.getBunchid()) || ( this.getBunchid()!=null && castOther.getBunchid()!=null && this.getBunchid().equals(castOther.getBunchid()) ) )
 && ( (this.getGuid0()==castOther.getGuid0()) || ( this.getGuid0()!=null && castOther.getGuid0()!=null && Arrays.equals(this.getGuid0(), castOther.getGuid0()) ) )
 && ( (this.getGuid1()==castOther.getGuid1()) || ( this.getGuid1()!=null && castOther.getGuid1()!=null && Arrays.equals(this.getGuid1(), castOther.getGuid1()) ) )
 && ( (this.getGuid2()==castOther.getGuid2()) || ( this.getGuid2()!=null && castOther.getGuid2()!=null && Arrays.equals(this.getGuid2(), castOther.getGuid2()) ) )
 && ( (this.getGuid0Char()==castOther.getGuid0Char()) || ( this.getGuid0Char()!=null && castOther.getGuid0Char()!=null && this.getGuid0Char().equals(castOther.getGuid0Char()) ) )
 && ( (this.getGuid1Char()==castOther.getGuid1Char()) || ( this.getGuid1Char()!=null && castOther.getGuid1Char()!=null && this.getGuid1Char().equals(castOther.getGuid1Char()) ) )
 && ( (this.getGuid2Char()==castOther.getGuid2Char()) || ( this.getGuid2Char()!=null && castOther.getGuid2Char()!=null && this.getGuid2Char().equals(castOther.getGuid2Char()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + (int) this.getDatasetId();
         result = 37 * result + (int) this.getEventnumber();
         result = 37 * result + ( getLumiblockn() == null ? 0 : this.getLumiblockn().hashCode() );
         result = 37 * result + ( getBunchid() == null ? 0 : this.getBunchid().hashCode() );
         result = 37 * result + ( getGuid0() == null ? 0 : Arrays.hashCode(this.getGuid0()) );
         result = 37 * result + ( getGuid1() == null ? 0 : Arrays.hashCode(this.getGuid1()) );
         result = 37 * result + ( getGuid2() == null ? 0 : Arrays.hashCode(this.getGuid2()) );
         result = 37 * result + ( getGuid0Char() == null ? 0 : this.getGuid0Char().hashCode() );
         result = 37 * result + ( getGuid1Char() == null ? 0 : this.getGuid1Char().hashCode() );
         result = 37 * result + ( getGuid2Char() == null ? 0 : this.getGuid2Char().hashCode() );
         return result;
   }   


}


