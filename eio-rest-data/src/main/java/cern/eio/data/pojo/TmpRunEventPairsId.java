package cern.eio.data.pojo;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * TmpRunEventPairsId generated by hbm2java
 */
@Embeddable
public class TmpRunEventPairsId  implements java.io.Serializable {


     private Long runnumber;
     private Long eventnumber;

    public TmpRunEventPairsId() {
    }

    public TmpRunEventPairsId(Long runnumber, Long eventnumber) {
       this.runnumber = runnumber;
       this.eventnumber = eventnumber;
    }
   

    @Column(name="RUNNUMBER", precision=10, scale=0)
    public Long getRunnumber() {
        return this.runnumber;
    }
    
    public void setRunnumber(Long runnumber) {
        this.runnumber = runnumber;
    }

    @Column(name="EVENTNUMBER", precision=10, scale=0)
    public Long getEventnumber() {
        return this.eventnumber;
    }
    
    public void setEventnumber(Long eventnumber) {
        this.eventnumber = eventnumber;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof TmpRunEventPairsId) ) return false;
		 TmpRunEventPairsId castOther = ( TmpRunEventPairsId ) other; 
         
		 return ( (this.getRunnumber()==castOther.getRunnumber()) || ( this.getRunnumber()!=null && castOther.getRunnumber()!=null && this.getRunnumber().equals(castOther.getRunnumber()) ) )
 && ( (this.getEventnumber()==castOther.getEventnumber()) || ( this.getEventnumber()!=null && castOther.getEventnumber()!=null && this.getEventnumber().equals(castOther.getEventnumber()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getRunnumber() == null ? 0 : this.getRunnumber().hashCode() );
         result = 37 * result + ( getEventnumber() == null ? 0 : this.getEventnumber().hashCode() );
         return result;
   }   


}


