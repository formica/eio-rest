package cern.eio.data.pojonotused;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * H2oDatafileInfo generated by hbm2java
 */
@Entity
@Table(name="H2O_DATAFILE_INFO"
    ,schema="ATLAS_EVENTINDEX"
)
public class H2oDatafileInfo  implements java.io.Serializable {


     private H2oDatafileInfoId id;

    public H2oDatafileInfo() {
    }

    public H2oDatafileInfo(H2oDatafileInfoId id) {
       this.id = id;
    }
   
     @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="datasetId", column=@Column(name="DATASET_ID", precision=10, scale=0) ), 
        @AttributeOverride(name="TStamp", column=@Column(name="T_STAMP", length=7) ), 
        @AttributeOverride(name="fileName", column=@Column(name="FILE_NAME", length=4000) ), 
        @AttributeOverride(name="rowsCount", column=@Column(name="ROWS_COUNT", precision=0) ), 
        @AttributeOverride(name="filesize", column=@Column(name="FILESIZE", precision=0) ) } )
    public H2oDatafileInfoId getId() {
        return this.id;
    }
    
    public void setId(H2oDatafileInfoId id) {
        this.id = id;
    }




}


