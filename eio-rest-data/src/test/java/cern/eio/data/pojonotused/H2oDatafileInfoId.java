package cern.eio.data.pojonotused;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * H2oDatafileInfoId generated by hbm2java
 */
@Embeddable
public class H2oDatafileInfoId  implements java.io.Serializable {


     private Long datasetId;
     private Date TStamp;
     private String fileName;
     private BigDecimal rowsCount;
     private BigDecimal filesize;

    public H2oDatafileInfoId() {
    }

    public H2oDatafileInfoId(Long datasetId, Date TStamp, String fileName, BigDecimal rowsCount, BigDecimal filesize) {
       this.datasetId = datasetId;
       this.TStamp = TStamp;
       this.fileName = fileName;
       this.rowsCount = rowsCount;
       this.filesize = filesize;
    }
   

    @Column(name="DATASET_ID", precision=10, scale=0)
    public Long getDatasetId() {
        return this.datasetId;
    }
    
    public void setDatasetId(Long datasetId) {
        this.datasetId = datasetId;
    }

    @Column(name="T_STAMP", length=7)
    public Date getTStamp() {
        return this.TStamp;
    }
    
    public void setTStamp(Date TStamp) {
        this.TStamp = TStamp;
    }

    @Column(name="FILE_NAME", length=4000)
    public String getFileName() {
        return this.fileName;
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(name="ROWS_COUNT", precision=0)
    public BigDecimal getRowsCount() {
        return this.rowsCount;
    }
    
    public void setRowsCount(BigDecimal rowsCount) {
        this.rowsCount = rowsCount;
    }

    @Column(name="FILESIZE", precision=0)
    public BigDecimal getFilesize() {
        return this.filesize;
    }
    
    public void setFilesize(BigDecimal filesize) {
        this.filesize = filesize;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof H2oDatafileInfoId) ) return false;
		 H2oDatafileInfoId castOther = ( H2oDatafileInfoId ) other; 
         
		 return ( (this.getDatasetId()==castOther.getDatasetId()) || ( this.getDatasetId()!=null && castOther.getDatasetId()!=null && this.getDatasetId().equals(castOther.getDatasetId()) ) )
 && ( (this.getTStamp()==castOther.getTStamp()) || ( this.getTStamp()!=null && castOther.getTStamp()!=null && this.getTStamp().equals(castOther.getTStamp()) ) )
 && ( (this.getFileName()==castOther.getFileName()) || ( this.getFileName()!=null && castOther.getFileName()!=null && this.getFileName().equals(castOther.getFileName()) ) )
 && ( (this.getRowsCount()==castOther.getRowsCount()) || ( this.getRowsCount()!=null && castOther.getRowsCount()!=null && this.getRowsCount().equals(castOther.getRowsCount()) ) )
 && ( (this.getFilesize()==castOther.getFilesize()) || ( this.getFilesize()!=null && castOther.getFilesize()!=null && this.getFilesize().equals(castOther.getFilesize()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getDatasetId() == null ? 0 : this.getDatasetId().hashCode() );
         result = 37 * result + ( getTStamp() == null ? 0 : this.getTStamp().hashCode() );
         result = 37 * result + ( getFileName() == null ? 0 : this.getFileName().hashCode() );
         result = 37 * result + ( getRowsCount() == null ? 0 : this.getRowsCount().hashCode() );
         result = 37 * result + ( getFilesize() == null ? 0 : this.getFilesize().hashCode() );
         return result;
   }   


}


