package cern.eio.data.pojonotused;
// Generated Apr 7, 2016 10:01:26 AM by Hibernate Tools 3.2.2.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * H2oDatasetInfo generated by hbm2java
 */
@Entity
@Table(name="H2O_DATASET_INFO"
    ,schema="ATLAS_EVENTINDEX"
    , uniqueConstraints = @UniqueConstraint(columnNames="SYS_NC00018$") 
)
public class H2oDatasetInfo  implements java.io.Serializable {


     private H2oDatasetInfoId id;

    public H2oDatasetInfo() {
    }

    public H2oDatasetInfo(H2oDatasetInfoId id) {
       this.id = id;
    }
   
     @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="datasetId", column=@Column(name="DATASET_ID", precision=10, scale=0) ), 
        @AttributeOverride(name="hadoopModifTime", column=@Column(name="HADOOP_MODIF_TIME", length=7) ), 
        @AttributeOverride(name="hadoopImpTime", column=@Column(name="HADOOP_IMP_TIME", length=7) ), 
        @AttributeOverride(name="h2oStartTime", column=@Column(name="H2O_START_TIME", length=7) ), 
        @AttributeOverride(name="h2oEndTime", column=@Column(name="H2O_END_TIME", length=7) ), 
        @AttributeOverride(name="filesCount", column=@Column(name="FILES_COUNT", precision=10, scale=0) ), 
        @AttributeOverride(name="h2oRowsCount", column=@Column(name="H2O_ROWS_COUNT", precision=10, scale=0) ), 
        @AttributeOverride(name="h2oStatus", column=@Column(name="H2O_STATUS", length=10) ), 
        @AttributeOverride(name="h2oLoadrateKhz", column=@Column(name="H2O_LOADRATE_KHZ", precision=0) ), 
        @AttributeOverride(name="schemaInfo", column=@Column(name="SCHEMA_INFO", length=4000) ), 
        @AttributeOverride(name="h2oMessage", column=@Column(name="H2O_MESSAGE", length=4000) ), 
        @AttributeOverride(name="h2oInfo", column=@Column(name="H2O_INFO", length=4000) ), 
        @AttributeOverride(name="oraStartTime", column=@Column(name="ORA_START_TIME", length=7) ), 
        @AttributeOverride(name="oraEndTime", column=@Column(name="ORA_END_TIME", length=7) ), 
        @AttributeOverride(name="oraStatus", column=@Column(name="ORA_STATUS", length=20) ), 
        @AttributeOverride(name="oraLoadrateKhz", column=@Column(name="ORA_LOADRATE_KHZ", precision=0) ), 
        @AttributeOverride(name="oraMessage", column=@Column(name="ORA_MESSAGE", length=4000) ) } )
    public H2oDatasetInfoId getId() {
        return this.id;
    }
    
    public void setId(H2oDatasetInfoId id) {
        this.id = id;
    }




}


